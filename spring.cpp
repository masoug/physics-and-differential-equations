#include <iostream>

using namespace std;

double k = 10.0;
double m = 1.0;
double b = 1.0;

double x = 3.0; // initial condition
double v = 1.0;
double h = 0.01;

double dvdt(double t) {
	return (-k/m)*x-(b/m)*v;
}

double acceleration(double t) {
	double k1 = dvdt(t);
	double k2 = dvdt(t);
	double k3 = dvdt(t);
	double k4 = dvdt(t);
	v = v +(h*(k1+(2.0*k2)+(2.0*k3)+k4)/6.0);
	return v;
}

double velocity() {
	double k1 = v;
	double k2 = v;
	double k3 = v;
	double k4 = v;
	x = x + (h*(k1+(2.0*k2)+(2.0*k3)+k4)/6.0);
	return x;
}

int main() {
	cout << "Time,x,v" << endl;
	for (double t = 0.0; t < 10.0; t += h) {
		cout << t << "," << velocity() << "," << acceleration(t) << endl;
	}
	return 0;
}