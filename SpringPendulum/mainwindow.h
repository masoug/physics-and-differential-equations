#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QGraphicsEllipseItem>
#include <QBrush>
#include <QPen>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_resetButton_clicked();

    void on_runSimulation_clicked(bool checked);

    void on_massSpinBox_valueChanged(double arg1);

    void on_springConstantSpinBox_valueChanged(double arg1);

    void on_gravitySpinBox_valueChanged(double arg1);

    void on_restLengthSpinBox_valueChanged(double arg1);

    void on_xSpinBox_valueChanged(double arg1);

    void on_ySpinBox_valueChanged(double arg1);

    void stepSimulation();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
    QGraphicsScene *scene;
    QGraphicsLineItem *rod;
    QGraphicsEllipseItem *bob;
    QPen *pen;
    QBrush *brush;

    /* Simulation parameters */
    double radius();
    double xAccel();
    double yAccel();
    void stepVelocity();
    void stepPosition();
    bool runSimulation;
    double x, y, vx, vy;
    double gravity, springConstant, mass, restLength;
};

#endif // MAINWINDOW_H
