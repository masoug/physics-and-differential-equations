#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>

#define STEP_SIZE 0.0166666

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->gravitySpinBox->setRange(-INFINITY, INFINITY);
    ui->restLengthSpinBox->setRange(-INFINITY, INFINITY);
    ui->xSpinBox->setRange(-INFINITY, INFINITY);
    ui->ySpinBox->setRange(-INFINITY, INFINITY);

    /* Setup scene */
    pen = new QPen(Qt::SolidLine);
    pen->setColor(Qt::black);
    pen->setWidth(3);
    brush = new QBrush(Qt::black);
    scene = new QGraphicsScene(this);
    rod = scene->addLine(0, 0, 0, 0, *pen);
    bob = scene->addEllipse(0, 0, 10, 10, *pen, *brush);
    ui->graphicsView->setScene(scene);

    /* Simulation params */
    runSimulation = false;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(stepSimulation()));
    timer->start(STEP_SIZE*1000);

    on_resetButton_clicked();
}

void MainWindow::stepSimulation() {
    if (runSimulation) {
        stepVelocity();
        stepPosition();
    }
    //update graphics...
    rod->setLine(0, 0, 50*x, 50*y);
    bob->setRect(50*x-5, 50*y-5, 10, 10);
}

double MainWindow::xAccel() {
    return -(springConstant*x)/(mass*radius())*(radius()-restLength);
}

double MainWindow::yAccel() {
    return (-(springConstant*y)/(mass*radius())*(radius()-restLength))+gravity;
}

void MainWindow::stepVelocity() {
    vx += STEP_SIZE*xAccel();
    vy += STEP_SIZE*yAccel();
}

void MainWindow::stepPosition() {
    x += STEP_SIZE*vx;
    y += STEP_SIZE*vy;
}

double MainWindow::radius() {
    return sqrt((x*x)+(y*y));
}

void MainWindow::on_resetButton_clicked()
{
    on_gravitySpinBox_valueChanged(ui->gravitySpinBox->value());
    on_springConstantSpinBox_valueChanged(ui->springConstantSpinBox->value());
    on_massSpinBox_valueChanged(ui->massSpinBox->value());
    on_restLengthSpinBox_valueChanged(ui->restLengthSpinBox->value());
    on_xSpinBox_valueChanged(ui->xSpinBox->value());
    on_ySpinBox_valueChanged(ui->ySpinBox->value());
    vx = vy = 0.0;
}

void MainWindow::on_runSimulation_clicked(bool checked)
{
    runSimulation = checked;
}

void MainWindow::on_massSpinBox_valueChanged(double arg1)
{
    mass = arg1;
}

void MainWindow::on_springConstantSpinBox_valueChanged(double arg1)
{
    springConstant = arg1;
}

void MainWindow::on_gravitySpinBox_valueChanged(double arg1)
{
    gravity = arg1;
}

void MainWindow::on_restLengthSpinBox_valueChanged(double arg1)
{
    restLength = arg1;
}

void MainWindow::on_xSpinBox_valueChanged(double arg1)
{
    x = arg1;
}

void MainWindow::on_ySpinBox_valueChanged(double arg1)
{
    y = arg1;
}

MainWindow::~MainWindow()
{
    delete ui;
}
