/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QFormLayout>
#include <QtGui/QGraphicsView>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QGraphicsView *graphicsView;
    QFormLayout *formLayout;
    QPushButton *resetButton;
    QPushButton *runSimulation;
    QLabel *initialPositionLabel;
    QHBoxLayout *horizontalLayout;
    QDoubleSpinBox *xSpinBox;
    QDoubleSpinBox *ySpinBox;
    QLabel *massLabel;
    QDoubleSpinBox *massSpinBox;
    QLabel *springConstantLabel;
    QDoubleSpinBox *springConstantSpinBox;
    QLabel *restLengthLabel;
    QDoubleSpinBox *restLengthSpinBox;
    QLabel *gravityLabel;
    QDoubleSpinBox *gravitySpinBox;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(390, 395);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));

        verticalLayout->addWidget(graphicsView);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        resetButton = new QPushButton(centralWidget);
        resetButton->setObjectName(QString::fromUtf8("resetButton"));

        formLayout->setWidget(1, QFormLayout::LabelRole, resetButton);

        runSimulation = new QPushButton(centralWidget);
        runSimulation->setObjectName(QString::fromUtf8("runSimulation"));
        runSimulation->setCheckable(true);
        runSimulation->setDefault(true);
        runSimulation->setFlat(false);

        formLayout->setWidget(1, QFormLayout::FieldRole, runSimulation);

        initialPositionLabel = new QLabel(centralWidget);
        initialPositionLabel->setObjectName(QString::fromUtf8("initialPositionLabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, initialPositionLabel);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        xSpinBox = new QDoubleSpinBox(centralWidget);
        xSpinBox->setObjectName(QString::fromUtf8("xSpinBox"));
        xSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        xSpinBox->setSingleStep(0.05);
        xSpinBox->setValue(0.2);

        horizontalLayout->addWidget(xSpinBox);

        ySpinBox = new QDoubleSpinBox(centralWidget);
        ySpinBox->setObjectName(QString::fromUtf8("ySpinBox"));
        ySpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        ySpinBox->setSingleStep(0.05);
        ySpinBox->setValue(0.2);

        horizontalLayout->addWidget(ySpinBox);


        formLayout->setLayout(2, QFormLayout::FieldRole, horizontalLayout);

        massLabel = new QLabel(centralWidget);
        massLabel->setObjectName(QString::fromUtf8("massLabel"));

        formLayout->setWidget(3, QFormLayout::LabelRole, massLabel);

        massSpinBox = new QDoubleSpinBox(centralWidget);
        massSpinBox->setObjectName(QString::fromUtf8("massSpinBox"));
        massSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        massSpinBox->setSingleStep(0.05);
        massSpinBox->setValue(0.5);

        formLayout->setWidget(3, QFormLayout::FieldRole, massSpinBox);

        springConstantLabel = new QLabel(centralWidget);
        springConstantLabel->setObjectName(QString::fromUtf8("springConstantLabel"));

        formLayout->setWidget(4, QFormLayout::LabelRole, springConstantLabel);

        springConstantSpinBox = new QDoubleSpinBox(centralWidget);
        springConstantSpinBox->setObjectName(QString::fromUtf8("springConstantSpinBox"));
        springConstantSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        springConstantSpinBox->setSingleStep(0.05);
        springConstantSpinBox->setValue(3);

        formLayout->setWidget(4, QFormLayout::FieldRole, springConstantSpinBox);

        restLengthLabel = new QLabel(centralWidget);
        restLengthLabel->setObjectName(QString::fromUtf8("restLengthLabel"));

        formLayout->setWidget(5, QFormLayout::LabelRole, restLengthLabel);

        restLengthSpinBox = new QDoubleSpinBox(centralWidget);
        restLengthSpinBox->setObjectName(QString::fromUtf8("restLengthSpinBox"));
        restLengthSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        restLengthSpinBox->setValue(0.25);

        formLayout->setWidget(5, QFormLayout::FieldRole, restLengthSpinBox);

        gravityLabel = new QLabel(centralWidget);
        gravityLabel->setObjectName(QString::fromUtf8("gravityLabel"));

        formLayout->setWidget(6, QFormLayout::LabelRole, gravityLabel);

        gravitySpinBox = new QDoubleSpinBox(centralWidget);
        gravitySpinBox->setObjectName(QString::fromUtf8("gravitySpinBox"));
        gravitySpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        gravitySpinBox->setValue(9.81);

        formLayout->setWidget(6, QFormLayout::FieldRole, gravitySpinBox);


        verticalLayout->addLayout(formLayout);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Spring-Pendulum", 0, QApplication::UnicodeUTF8));
        resetButton->setText(QApplication::translate("MainWindow", "Reset", 0, QApplication::UnicodeUTF8));
        runSimulation->setText(QApplication::translate("MainWindow", "Run Simulation!", 0, QApplication::UnicodeUTF8));
        initialPositionLabel->setText(QApplication::translate("MainWindow", "Position (x,y)", 0, QApplication::UnicodeUTF8));
        xSpinBox->setSuffix(QApplication::translate("MainWindow", "m", 0, QApplication::UnicodeUTF8));
        ySpinBox->setSuffix(QApplication::translate("MainWindow", "m", 0, QApplication::UnicodeUTF8));
        massLabel->setText(QApplication::translate("MainWindow", "Mass", 0, QApplication::UnicodeUTF8));
        massSpinBox->setSuffix(QApplication::translate("MainWindow", "kg", 0, QApplication::UnicodeUTF8));
        springConstantLabel->setText(QApplication::translate("MainWindow", "Spring Constant", 0, QApplication::UnicodeUTF8));
        springConstantSpinBox->setSuffix(QApplication::translate("MainWindow", "N/m", 0, QApplication::UnicodeUTF8));
        restLengthLabel->setText(QApplication::translate("MainWindow", "Rest Length", 0, QApplication::UnicodeUTF8));
        restLengthSpinBox->setSuffix(QApplication::translate("MainWindow", "m", 0, QApplication::UnicodeUTF8));
        gravityLabel->setText(QApplication::translate("MainWindow", "Gravity", 0, QApplication::UnicodeUTF8));
        gravitySpinBox->setSuffix(QApplication::translate("MainWindow", "m/s/s", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
