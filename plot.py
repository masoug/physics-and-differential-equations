import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from ftplib import FTP
from string import find
import csv
import sys

# Download data file.
def download(ip, path):
    # Connect to the robot...
    #print "Connecting to robot @", ip
    robot = FTP(ip)
    #print "Logging in as anonymous..."
    robot.login()
    # print "ROOT DIRECTORY:"
    # robot.retrlines("LIST")
    print "Retrieving", path, "..."
    robot.retrbinary("RETR "+path, open(path, "wb").write)
    robot.quit()
    #print "DONE"

# Remove last line in data file.
def trim(path):
    lines = open(path).readlines()
    open(path, 'w').writelines(lines[:-1])

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage:"
        print "\tplot.py <ipaddress> <file1> <file2> <file3> ..."
        print "\n\tDownloaded files must be in CSV format!"
        exit(-1)

    # Get the data files!
    server = sys.argv[1]
    dataFiles = sys.argv[2:]
    numSubplots = len(dataFiles)

    # loop through the files and plot them!
    for i,fp in list(enumerate(dataFiles)):
        if server != "null":
            download(server, fp)
            trim(fp)

        # Generate the plot
        plt.subplot(numSubplots, 1, i+1)
        data = mlab.csv2rec(fp, "#", 0, 0, ",")
        for field in data.dtype.names[1:]:
            plt.plot(data.time, data[field], label=field)
            plt.ylabel("Value")
            plt.legend()

    # Show the plot!
    plt.xlabel("Time (seconds)")
    plt.show()
