/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QFormLayout>
#include <QtGui/QGraphicsView>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QGraphicsView *graphicsView;
    QHBoxLayout *horizontalLayout;
    QFormLayout *formLayout;
    QLabel *angleALabel;
    QDoubleSpinBox *angleA;
    QLabel *lengthALabel;
    QDoubleSpinBox *lengthA;
    QLabel *lengthBLabel;
    QDoubleSpinBox *lengthB;
    QLabel *angleBLabel;
    QDoubleSpinBox *angleB;
    QCheckBox *runSimluation;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(422, 314);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));

        verticalLayout->addWidget(graphicsView);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        angleALabel = new QLabel(centralWidget);
        angleALabel->setObjectName(QString::fromUtf8("angleALabel"));

        formLayout->setWidget(1, QFormLayout::LabelRole, angleALabel);

        angleA = new QDoubleSpinBox(centralWidget);
        angleA->setObjectName(QString::fromUtf8("angleA"));
        angleA->setSingleStep(0.5);
        angleA->setValue(45);

        formLayout->setWidget(1, QFormLayout::FieldRole, angleA);

        lengthALabel = new QLabel(centralWidget);
        lengthALabel->setObjectName(QString::fromUtf8("lengthALabel"));

        formLayout->setWidget(2, QFormLayout::LabelRole, lengthALabel);

        lengthA = new QDoubleSpinBox(centralWidget);
        lengthA->setObjectName(QString::fromUtf8("lengthA"));
        lengthA->setMaximum(50);
        lengthA->setSingleStep(0.5);
        lengthA->setValue(0.5);

        formLayout->setWidget(2, QFormLayout::FieldRole, lengthA);

        lengthBLabel = new QLabel(centralWidget);
        lengthBLabel->setObjectName(QString::fromUtf8("lengthBLabel"));

        formLayout->setWidget(4, QFormLayout::LabelRole, lengthBLabel);

        lengthB = new QDoubleSpinBox(centralWidget);
        lengthB->setObjectName(QString::fromUtf8("lengthB"));
        lengthB->setWrapping(false);
        lengthB->setFrame(true);
        lengthB->setButtonSymbols(QAbstractSpinBox::UpDownArrows);
        lengthB->setAccelerated(false);
        lengthB->setMaximum(50);
        lengthB->setSingleStep(0.5);
        lengthB->setValue(0.5);

        formLayout->setWidget(4, QFormLayout::FieldRole, lengthB);

        angleBLabel = new QLabel(centralWidget);
        angleBLabel->setObjectName(QString::fromUtf8("angleBLabel"));

        formLayout->setWidget(3, QFormLayout::LabelRole, angleBLabel);

        angleB = new QDoubleSpinBox(centralWidget);
        angleB->setObjectName(QString::fromUtf8("angleB"));
        angleB->setSingleStep(0.5);
        angleB->setValue(45);

        formLayout->setWidget(3, QFormLayout::FieldRole, angleB);


        horizontalLayout->addLayout(formLayout);

        runSimluation = new QCheckBox(centralWidget);
        runSimluation->setObjectName(QString::fromUtf8("runSimluation"));

        horizontalLayout->addWidget(runSimluation);


        verticalLayout->addLayout(horizontalLayout);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        angleALabel->setText(QApplication::translate("MainWindow", "Angle A:", 0, QApplication::UnicodeUTF8));
        angleA->setSuffix(QApplication::translate("MainWindow", "\302\260", 0, QApplication::UnicodeUTF8));
        lengthALabel->setText(QApplication::translate("MainWindow", "Length A:", 0, QApplication::UnicodeUTF8));
        lengthA->setSuffix(QApplication::translate("MainWindow", "m", 0, QApplication::UnicodeUTF8));
        lengthBLabel->setText(QApplication::translate("MainWindow", "Length B:", 0, QApplication::UnicodeUTF8));
        lengthB->setSuffix(QApplication::translate("MainWindow", "m", 0, QApplication::UnicodeUTF8));
        angleBLabel->setText(QApplication::translate("MainWindow", "Angle B:", 0, QApplication::UnicodeUTF8));
        angleB->setSuffix(QApplication::translate("MainWindow", "\302\260", 0, QApplication::UnicodeUTF8));
        runSimluation->setText(QApplication::translate("MainWindow", "Run Simulation", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
