#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>
#include <iostream>

#define MASS_WIDTH 10
#define TIME_INTERVAL 0.01666666666 // 1/60th of a second
#define G 9.81

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    runSimulation(false)
{
    ui->setupUi(this);
    ui->angleA->setRange(-INFINITY, INFINITY);
    ui->angleB->setRange(-INFINITY, INFINITY);

    angleA = ui->angleA->value();
    angleB = ui->angleB->value();
    lengthA = ui->lengthA->value();
    lengthB = ui->lengthB->value();
    velocityA = 0.0;
    velocityB = 0.0;

    stringPen = new QPen(Qt::black);
    stringPen->setWidth(2);
    stringPen->setCapStyle(Qt::RoundCap);
    massBrush = new QBrush(Qt::black, Qt::SolidPattern);

    scene = new QGraphicsScene(this);
    lA = scene->addLine(0, 0,
                        100*lengthA*sin(angleA), 100*lengthA*cos(angleA),
                        *stringPen);
    mA = scene->addEllipse(100*lengthA*sin(angleA), 100*lengthA*cos(angleA),
                           MASS_WIDTH, MASS_WIDTH, *stringPen, *massBrush);
    lB = scene->addLine(100*lengthA*sin(angleA), 100*lengthA*cos(angleA),
                        100*(lengthA*sin(angleA)+lengthB*sin(angleB)),
                        100*(lengthA*cos(angleA)+lengthB*cos(angleB)),
                        *stringPen);
    mB = scene->addEllipse(100*(lengthA*sin(angleA)+lengthB*sin(angleB)),
                           100*(lengthA*cos(angleA)+lengthB*cos(angleB)),
                           MASS_WIDTH, MASS_WIDTH, *stringPen, *massBrush);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->show();

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(stepSimulation()));
    timer->start(TIME_INTERVAL*1000);

    setWindowTitle(tr("Pendulums!"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_runSimluation_clicked(bool checked)
{
    runSimulation = checked;
    velocityA = 0.0;
    velocityB = 0.0;
}

double MainWindow::dvdtA() {
    double A = -3*G*sin(angleA);
    double B = -G*sin(angleA-2*angleB);
    double CA = -2*sin(angleA-angleB);
    double CB = (velocityB*velocityB)*lengthB;
    double CC = (velocityA*velocityA)*lengthA*cos(angleA-angleB);
    double C = CA*(CB+CC);
    double D = lengthA*(3-cos(2*angleA-2*angleB));
    return (A+B+C)/D;
}

double MainWindow::dvdtB() {
    double A = 2*sin(angleA-angleB)*((velocityA*velocityA)*lengthA*2+G*2*cos(angleA)+(velocityB*velocityB)*lengthB*cos(angleA-angleB));
    double B = lengthB*(3-cos(2*angleA-2*angleB));
    return A/B;
}

void MainWindow::velocity() {
    // integrate acceleration
    double k1A = dvdtA();
    double k2A = dvdtA();
    double k3A = dvdtA();
    double k4A = dvdtA();
    velocityA = velocityA +(TIME_INTERVAL*(k1A+(2.0*k2A)+(2.0*k3A)+k4A)/6.0);
    double k1B = dvdtB();
    double k2B = dvdtB();
    double k3B = dvdtB();
    double k4B = dvdtB();
    velocityB = velocityB +(TIME_INTERVAL*(k1B+(2.0*k2B)+(2.0*k3B)+k4B)/6.0);
}

void MainWindow::angle() {
    double k1 = velocityA;
    double k2 = velocityA;
    double k3 = velocityA;
    double k4 = velocityA;
    angleA = angleA + (TIME_INTERVAL*(k1+(2.0*k2)+(2.0*k3)+k4)/6.0);
    double k1B = velocityB;
    double k2B = velocityB;
    double k3B = velocityB;
    double k4B = velocityB;
    angleB = angleB + (TIME_INTERVAL*(k1B+(2.0*k2B)+(2.0*k3B)+k4B)/6.0);
}

void MainWindow::stepSimulation() {
    if (runSimulation) {
        velocity();
        angle();
    }
    //update the simulation parameters here
    lA->setLine(0, 0, 100*lengthA*sin(angleA), 100*lengthA*cos(angleA));
    lB->setLine(100*lengthA*sin(angleA), 100*lengthA*cos(angleA),
                100*(lengthA*sin(angleA)+lengthB*sin(angleB)),
                100*(lengthA*cos(angleA)+lengthB*cos(angleB)));
    mB->setRect(100*(lengthA*sin(angleA)+lengthB*sin(angleB))-(MASS_WIDTH/2),
                100*(lengthA*cos(angleA)+lengthB*cos(angleB))-(MASS_WIDTH/2),
                MASS_WIDTH, MASS_WIDTH);
    mA->setRect(100*lengthA*sin(angleA)-(MASS_WIDTH/2),
                100*lengthA*cos(angleA)-(MASS_WIDTH/2),
                MASS_WIDTH, MASS_WIDTH);
}

void MainWindow::on_angleA_valueChanged(double arg1)
{
    angleA = arg1*M_PI/180;
}

void MainWindow::on_lengthA_valueChanged(double arg1)
{
    lengthA = arg1;
}

void MainWindow::on_angleB_valueChanged(double arg1)
{
    angleB = arg1*M_PI/180;
}

void MainWindow::on_lengthB_valueChanged(double arg1)
{
    lengthB = arg1;
}
