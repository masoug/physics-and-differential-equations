#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QPen>
#include <QBrush>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_runSimluation_clicked(bool checked);
    void stepSimulation();

    void on_angleA_valueChanged(double arg1);

    void on_lengthA_valueChanged(double arg1);

    void on_angleB_valueChanged(double arg1);

    void on_lengthB_valueChanged(double arg1);

private:
    Ui::MainWindow *ui;
    QTimer *timer;

    /* Simulation items */
    QGraphicsScene *scene;
    QGraphicsLineItem *lA;
    QGraphicsEllipseItem *mA;
    QGraphicsLineItem *lB;
    QGraphicsEllipseItem *mB;
    QPen *stringPen;
    QBrush *massBrush;


    /* Simulation params. */
    double dvdtA();
    double dvdtB();
    void velocity();
    void angle();
    double time;
    double velocityA, angleA, lengthA;
    double velocityB, angleB, lengthB;
    bool runSimulation;
};

#endif // MAINWINDOW_H
